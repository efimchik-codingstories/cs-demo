package com.epam.csdemo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortingTest {

    @Test
    void testRegularCase() {
        int[] array = {5, 4, 8, 6, 3};
        new Sorting().sortArray(array);

        assertArrayEquals(array, new int[]{3, 4, 5, 6, 8});
    }

    @Test
    void testNullCase() {
        assertThrows(IllegalArgumentException.class, () -> new Sorting().sortArray(null)); // fails
    }
}